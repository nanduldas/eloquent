<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', \App\Http\Controllers\HomeController::class);
Route::get('/generate', [\App\Http\Controllers\generateController::class,'generateFakeData']);
Route::get('/hasone', [\App\Http\Controllers\HomeController::class,'oneToOne']);
