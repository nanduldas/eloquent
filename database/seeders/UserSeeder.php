<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->insert([
//            'name' => Str::random(10),
//            'email' => Str::random(10).'@gmail.com',
//            'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
//            'remember_token' =>Str::random(10),
//            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
//            'updated_at' =>Carbon::now()->format('Y-m-d H:i:s'),
//        ]);

        User::factory()->count(100)->create();
    }
}
