<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
            return view('welcome');

    }
    public function oneToOne(Request $request)
    {
        $user = User::first();
        $data = $user;
            return view('welcome', ['data' => $data]);

    }

}
